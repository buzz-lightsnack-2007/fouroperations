/* fouroperations.js
Solve the four operations. */

function fouroperations(number1, number2) {
	const inputs_numbers = [number1, number2];
	
	const operationResults = {
		'sum': null, 
		'difference': null, 
		'product': null, 
		'quotient': null, 
		'exponential': null
	};
	
	/* Solve the data given. */
	operationResults.sum = (inputs_numbers[0] + inputs_numbers[1]);
	operationResults.difference = (inputs_numbers[0] - inputs_numbers[1]);
	operationResults.product = (inputs_numbers[0] * inputs_numbers[1]);
	operationResults.exponential = (inputs_numbers[0] ** inputs_numbers[1]);
	
	try {
		operationResults.quotient = (number1 / number2);
	} catch(err) {
		operationResults.quotient = null;
	};
	
	return (operationResults);
};

function displayAnswers(number1 = null, number2 = null) {
	/* Update the numbers properly. */
	if (number1 != null) {document.getElementById("input_number_1").value = number1};
	if (number2 != null) {document.getElementById("input_number_2").value = number2};
	
	/* Register the numbers now. */
	const inputs_numbers = [parseFloat(document.getElementById("input_number_1").value), parseFloat(document.getElementById("input_number_2").value)];
	
	if ((inputs_numbers[0]) && (inputs_numbers[1])) {
		/* Solve the numbers. */
		operationResults = fouroperations(inputs_numbers[0], inputs_numbers[1]);
		
		/* Update the display output. */
		document.getElementById("output_number_sum").value = operationResults.sum;
		document.getElementById("output_number_difference").value = operationResults.difference;
		document.getElementById("output_number_product").value = operationResults.product;
		document.getElementById("output_number_quotient").value = operationResults.quotient;
		document.getElementById("output_number_exponential").value = operationResults.exponential;
		
		/* Return the values. */
		return (operationResults);
	};
}